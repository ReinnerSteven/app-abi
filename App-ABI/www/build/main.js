webpackJsonp([0],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return RequestType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return ServicesModel; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Tipos de servicios que se van a realizar
 */
var RequestModel;
(function (RequestModel) {
    RequestModel["GET"] = "GET";
    RequestModel["PUT"] = "PUT";
    RequestModel["POST"] = "POST";
})(RequestModel || (RequestModel = {}));
/**
 * Enum enfocado a los tipos de servicios que se van hacer
 */
var RequestType;
(function (RequestType) {
    //SALEFORCE
    RequestType[RequestType["SALEFORCE_RESET_PASS"] = 0] = "SALEFORCE_RESET_PASS";
    //SAP
    RequestType[RequestType["SAP_UNLOCK_USER"] = 1] = "SAP_UNLOCK_USER";
    RequestType[RequestType["SAP_VALIDATE_USER"] = 2] = "SAP_VALIDATE_USER"; //Valor 2
})(RequestType || (RequestType = {}));
var ServicesModel = /** @class */ (function () {
    function ServicesModel() {
    }
    /**
     * Funcion que se encarga de validar que tipo de solicitud se va a realizar
     * @param {RequestType}  typeServcie Tipo de SERVICIO que se quiere realizar ( endpoints)
     * @param {QueryModel}  query Parametros para agregar a la url, estos deben de estar igual
     * al que se implementa en la interface. si este campo no va, colocar null
     * @returns {String} Returns url para realizar el servicio
     */
    ServicesModel.prototype.urlRequest = function (typeServcie, query) {
        console.log(typeServcie);
        switch (typeServcie) {
            case 0: {
                return "https://caro-chatboot.de-c1.cloudhub.io/api/1.1/accounts/" + query.userId + "/resetPassword";
            }
            case 1: {
                return "https://caro-chatboot.de-c1.cloudhub.io/api/1.1/employees/" + query.userId + "/unblock";
            }
            case 2: {
                return "https://caro-chatboot.de-c1.cloudhub.io/api/1.1/employees/" + query.userId + "/validate";
            }
            default: {
                console.log("Invalid endpoint");
                break;
            }
        }
    };
    ServicesModel = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], ServicesModel);
    return ServicesModel;
}());

//# sourceMappingURL=model.request.js.map

/***/ }),

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 154;

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_device_location_device_location__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_device_camera_device_camera__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_request_request__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_model_request__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_formdinamico__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_renewpwdSFDC__ = __webpack_require__(286);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, _camera, _geolocation, _request, _service, _renewpwdSF, loadingCtrl) {
        this.navCtrl = navCtrl;
        this._camera = _camera;
        this._geolocation = _geolocation;
        this._request = _request;
        this._service = _service;
        this._renewpwdSF = _renewpwdSF;
        this.loadingCtrl = loadingCtrl;
        console.log("--> Start Home\n");
        this.inputdinamico = _service.getQuestions();
        this.formrenewpwdSF = _renewpwdSF.getQuestions();
    }
    HomePage.prototype.openCamera = function (openCamera) {
        var _this = this;
        console.log(openCamera);
        this._camera.openCamera(openCamera).then(function (src) {
            _this.imagePath = src;
        }).catch(function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.loadlocation = function () {
        var _this = this;
        this._geolocation.currentLocation().then(function (location) {
            _this.locationUser = location;
        }).catch(function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.unblockSAP = function (event) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        var query = event;
        var data = {
            "metaData": {
                "msgId": "XMKMSIJD",
                "user": "test",
                "source": "app/Web/ChatBot",
                "target": "PR3/HCM/SF/Compaxx/SharpOps/ServiceNow/Datamart",
                "transactionDate": "2017-08-10T09:05:49.999"
            },
            "data": []
        };
        this._request.request(__WEBPACK_IMPORTED_MODULE_5__models_model_request__["a" /* RequestModel */].PUT, __WEBPACK_IMPORTED_MODULE_5__models_model_request__["b" /* RequestType */].SAP_UNLOCK_USER, data, query)
            .then(function (data) {
            loading.dismiss();
            _this.result = JSON.stringify(data.body.data);
        })
            .catch(function (error) {
            console.log(error);
            loading.dismiss();
        });
    };
    HomePage.prototype.resetPasswordSFDC = function (event) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        console.log("reset password");
        var query = event;
        var data = {
            "metaData": {
                "msgId": "XMKMSIJD",
                "user": "test",
                "source": "app/Web/ChatBot",
                "target": "PR3/HCM/SF/Compaxx/SharpOps/ServiceNow/Datamart",
                "transactionDate": "2017-08-10T09:05:49.999"
            },
            "data": []
        };
        this._request.request(__WEBPACK_IMPORTED_MODULE_5__models_model_request__["a" /* RequestModel */].PUT, __WEBPACK_IMPORTED_MODULE_5__models_model_request__["b" /* RequestType */].SALEFORCE_RESET_PASS, data, query)
            .then(function (data) {
            loading.dismiss();
            console.log(data);
            _this.result = JSON.stringify(data.body.data);
        })
            .catch(function (error) {
            console.log(error);
            loading.dismiss();
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar color="secondary">\n    <ion-title>\n      Applicaicon ABI\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n\n  <ion-card showWhen="android,ios">\n    <ion-card-header>\n      <h2>Camera</h2>\n    </ion-card-header>\n    <ion-card-content>\n        <button ion-button full (click)="openCamera(true)">Camera</button>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card showWhen="android,ios">\n    <ion-card-header>\n      <h2>Gallery</h2>\n    </ion-card-header>\n    <ion-card-content>\n        <button ion-button full (click)="openCamera(false)">Gallery</button>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card  showWhen="android,ios" *ngIf="imagePath">\n    <ion-card-content>\n        <img [src]="imagePath" alt="">\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      <h2>Location</h2>\n    </ion-card-header>\n    <ion-card-content>\n        <button ion-button full (click)="loadlocation(true)">location</button>\n        <p *ngIf="locationUser">{{locationUser.timestamp}}</p>\n    </ion-card-content>\n  </ion-card>  \n\n\n\n  \n  <ion-card>\n    <ion-card-header>\n      <h2>Desbloqueo  Empleado SAP</h2>\n    </ion-card-header>\n    <ion-card-content>\n      <form-dynamic [inputdinamico]="inputdinamico"  (values)="unblockSAP($event)"></form-dynamic>\n      <p *ngIf=\'result\'>{{result}}</p>\n    </ion-card-content>\n  </ion-card>  \n\n  <ion-card>\n    <ion-card-header>\n      <h2>Reset Password Usuario SFCD</h2>\n    </ion-card-header>\n    <ion-card-content>\n      <form-dynamic [inputdinamico]="formrenewpwdSF"  (values)="resetPasswordSFDC($event)"></form-dynamic>\n      <p *ngIf=\'result\'>{{result}}</p>\n    </ion-card-content>    \n  </ion-card>  \n  \n</ion-content>\n'/*ion-inline-end:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__providers_device_camera_device_camera__["a" /* DeviceCameraProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_device_camera_device_camera__["a" /* DeviceCameraProvider */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__providers_device_location_device_location__["a" /* DeviceLocationProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_device_location_device_location__["a" /* DeviceLocationProvider */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__providers_request_request__["a" /* RequestProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_request_request__["a" /* RequestProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__services_formdinamico__["a" /* QuestionService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__services_formdinamico__["a" /* QuestionService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_7__services_renewpwdSFDC__["a" /* ReNewPasswordSF */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__services_renewpwdSFDC__["a" /* ReNewPasswordSF */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* LoadingController */]) === "function" && _g || Object])
    ], HomePage);
    return HomePage;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceLocationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_geolocation__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DeviceLocationProvider = /** @class */ (function () {
    function DeviceLocationProvider(_geolocation) {
        this._geolocation = _geolocation;
    }
    /**
     * Funcion que se encarga de optener la posicion actual del usuario   *
     * @returns {Promise<any>} Returns una Promise que contiene la posicion del usuario y
     * el timestamp de donde se solicito
    */
    DeviceLocationProvider.prototype.currentLocation = function () {
        var _this = this;
        var prom = new Promise(function (resolve, reflect) {
            _this._geolocation.getCurrentPosition().then(function (resp) {
                resolve(resp);
            }).catch(function (err) {
                reflect(err);
            });
        });
        return prom;
    };
    DeviceLocationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__ionic_native_geolocation__["a" /* Geolocation */]])
    ], DeviceLocationProvider);
    return DeviceLocationProvider;
}());

//# sourceMappingURL=device-location.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceCameraProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_camera__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DeviceCameraProvider = /** @class */ (function () {
    function DeviceCameraProvider(camera) {
        this.camera = camera;
        /**
        * Variable que contiene la configuracion para cargar la camara
       */
        this.options = {
            quality: 100,
            correctOrientation: true,
            mediaType: this.camera.MediaType.PICTURE,
            encodingType: this.camera.EncodingType.JPEG,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
    }
    /**
     * Funcion que se encarga de activar el acceso a la camara
     * @param { Boolean}  [openGallery]  Define si se carga la camara o la galeria
     * @returns {Promise<any>} Returns una Promise la imagen en base 64
     * el timestamp de donde se solicito
    */
    DeviceCameraProvider.prototype.openCamera = function (openGallery) {
        var _this = this;
        var prom = new Promise(function (resolve, reflect) {
            _this.options.sourceType = !openGallery ? _this.camera.PictureSourceType.PHOTOLIBRARY : _this.camera.PictureSourceType.CAMERA;
            _this.camera.getPicture(_this.options).then(function (imageData) {
                resolve("data:image/jpeg;base64," + imageData);
            }, function (err) {
                reflect(err);
            });
        });
        return prom;
    };
    DeviceCameraProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_camera__["a" /* Camera */]])
    ], DeviceCameraProvider);
    return DeviceCameraProvider;
}());

//# sourceMappingURL=device-camera.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_model_request__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestProvider = /** @class */ (function () {
    function RequestProvider(_httpClient, _servicesModel) {
        this._httpClient = _httpClient;
        this._servicesModel = _servicesModel;
        this._headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({
            "Authorization": "Basic YWRtaW46YWRtaW4=",
            "Content-Type": "application/json"
        });
    }
    /**
    * Funcion que se encarga de realizar una peticion HTTP al servidor
    * @param {RequestModel}  typeRequest Tipo de SOLICITUD que se quiere realizar GET,POST, PUT
    * @param {RequestType}  typeServcie Tipo de SERVICIO que se quiere realizar ( endpoints)
    * @param {any}  body Parametros tipos JSON para enviar en la solicitud. si este campo no va, colocar null
    * @param {QueryModel}  query Parametros para agregar a la url, estos deben de estar igual
    * al que se implementa en la interface. si este campo no va, colocar null
    * @returns {Promise<any>} Returns una Promise que contiene la posicion del usuario y
    * el timestamp de donde se solicito
   */
    RequestProvider.prototype.request = function (typeRequest, typeServcie, body, query) {
        var url = this._servicesModel.urlRequest(typeServcie, query);
        console.log("\n---> RequestModel: ", typeRequest);
        console.log("\n---> URL: ", url);
        console.log("\n---> PARAMS: ", body);
        console.log("\n---> HEADERS: ", this._headers);
        console.log("\n---> QUERY: ", query);
        var request = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpRequest */](typeRequest, url, JSON.stringify(body), {
            headers: this._headers,
        });
        return this._httpClient.request(request).toPromise();
    };
    /**
   * Funcion que se encarga de realizar una peticion HTTP al servidor
   * @param {RequestModel}  typeRequest Tipo de SOLICITUD que se quiere realizar GET,POST, PUT
   * @param {RequestType}  typeServcie Tipo de SERVICIO que se quiere realizar ( endpoints)
   * @param {any}  body Parametros tipos JSON para enviar en la solicitud
   * @param {QueryModel}  query Parametros para agregar a la url, estos deben de estar igual
   * al que se implementa en la interface
   *
   *
   * Para implementar este metodo de progreso debemos suscribirnos a los eventos
   *
   *  .subscribe(event => {
        if (event.type === HttpEventType.DownloadProgress) {
          let port = (event.loaded / event.total) * 100
          console.log(port);
        }
        
        if (event.type === HttpEventType.UploadProgress) {
          let port = (event.loaded / event.total) * 100
          console.log(port);
        }

        if (event.type === HttpEventType.Response) {
          console.log(event.body);
        }
      }, (error:HttpErrorResponse) =>{
        console.log(error);
      })
   *
   *
   * @returns {Promise<any>} Returns una Promise que contiene la posicion del usuario y
   * el timestamp de donde se solicito
  */
    RequestProvider.prototype.requestProgress = function (typeRequest, typeServcie, body, query) {
        var url = this._servicesModel.urlRequest(typeServcie, query);
        var request = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpRequest */](typeRequest, url, {
            headers: this._headers
        });
        return this._httpClient.request(request);
    };
    RequestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__models_model_request__["c" /* ServicesModel */]])
    ], RequestProvider);
    return RequestProvider;
}());

//# sourceMappingURL=request.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(225);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//export PATH=$PATH:/opt/gradle/gradle-4.9/bin
//./gradlew wrapper --gradle-version=4.9 --distribution-type=bin 
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_device_camera_device_camera__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_device_location_device_location__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_request_request__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_http__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__models_model_request__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_input_dynamic_input_dynamic__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_form_dynamic_form_dynamic__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_common__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_renewpwdSFDC__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_formdinamico__ = __webpack_require__(287);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//Device









//Dynamic Form






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_16__components_form_dynamic_form_dynamic__["a" /* FormDynamicComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_input_dynamic_input_dynamic__["a" /* InputDynamicComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_17__angular_forms__["c" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_17__angular_forms__["f" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_18__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_9__providers_device_camera_device_camera__["a" /* DeviceCameraProvider */],
                __WEBPACK_IMPORTED_MODULE_10__providers_device_location_device_location__["a" /* DeviceLocationProvider */],
                __WEBPACK_IMPORTED_MODULE_11__providers_request_request__["a" /* RequestProvider */],
                __WEBPACK_IMPORTED_MODULE_14__models_model_request__["c" /* ServicesModel */],
                __WEBPACK_IMPORTED_MODULE_20__services_formdinamico__["a" /* QuestionService */],
                __WEBPACK_IMPORTED_MODULE_19__services_renewpwdSFDC__["a" /* ReNewPasswordSF */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            console.log("--> Start App \n");
            //statusBar.overlaysWebView(true);
            statusBar.backgroundColorByHexString('#0B3B39');
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputBase; });
var InputBase = /** @class */ (function () {
    function InputBase(options) {
        if (options === void 0) { options = {}; }
        this.value = options.value;
        this.key = options.key || '';
        this.label = options.label || '';
        this.required = !!options.required;
        this.order = options.order === undefined ? 1 : options.order;
        this.controlType = options.controlType || '';
        this.pattern = options.pattern || '';
        this.llave = options.llave || '';
    }
    return InputBase;
}());

//# sourceMappingURL=inputs.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TextboxQuestion; });
/* unused harmony export DropdownQuestion */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__inputs__ = __webpack_require__(281);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
  INPUT ETN
  E- EMAIL
  T- TEXT
  N- NUMBER
  DEFINIMOS COMO SE COMPORTARA LA ESTRUCTURA DE ESTE TIPO
  DE INPUTS
  Y LOS TIPO SELECT
*/

var TextboxQuestion = /** @class */ (function (_super) {
    __extends(TextboxQuestion, _super);
    function TextboxQuestion(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.controlType = 'textbox';
        _this.type = options['type'] || '';
        return _this;
    }
    return TextboxQuestion;
}(__WEBPACK_IMPORTED_MODULE_0__inputs__["a" /* InputBase */]));

var DropdownQuestion = /** @class */ (function (_super) {
    __extends(DropdownQuestion, _super);
    function DropdownQuestion(options) {
        if (options === void 0) { options = {}; }
        var _this = _super.call(this, options) || this;
        _this.controlType = 'dropdown';
        _this.options = [];
        _this.options = options['options'] || [];
        return _this;
    }
    return DropdownQuestion;
}(__WEBPACK_IMPORTED_MODULE_0__inputs__["a" /* InputBase */]));

//# sourceMappingURL=input-etn.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDynamicComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_types_inputs_inputs__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InputDinamicoComponent component.
 *
 * NOS PERMITE DEFINIR EL COMPORTAMIENTO QUE TENDRA
 * DEPENDIENDO DEL INPUT QUE SE INGRESE
 */
var InputDynamicComponent = /** @class */ (function () {
    function InputDynamicComponent() {
    }
    Object.defineProperty(InputDynamicComponent.prototype, "isValid", {
        get: function () { return this.form.controls[this.inputdinamico.key].valid; },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__providers_types_inputs_inputs__["a" /* InputBase */])
    ], InputDynamicComponent.prototype, "inputdinamico", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */])
    ], InputDynamicComponent.prototype, "form", void 0);
    InputDynamicComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'input-dinamico',template:/*ion-inline-start:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/components/input-dynamic/input-dynamic.html"*/'<form [formGroup]="form">\n\n  \n\n  <ion-item [ngSwitch]="inputdinamico.controlType">\n      <ion-label *ngSwitchCase="\'textbox\'">{{inputdinamico.label}}</ion-label>\n    <ion-input *ngSwitchCase="\'textbox\'" [formControlName]="inputdinamico.key"\n            [id]="inputdinamico.key" [type]="inputdinamico.type"\n            [pattern]="inputdinamico.pattern"></ion-input>\n           \n           \n    <select [id]="inputdinamico.key" *ngSwitchCase="\'dropdown\'" [formControlName]="inputdinamico.key">\n      <option *ngFor="let opt of inputdinamico.options" [value]="opt.key">{{opt.value}}</option>\n    </select>\n\n  </ion-item>\n\n  <div *ngIf="!isValid">{{inputdinamico.label}} is required</div>\n</form>\n\n'/*ion-inline-end:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/components/input-dynamic/input-dynamic.html"*/
        })
    ], InputDynamicComponent);
    return InputDynamicComponent;
}());

//# sourceMappingURL=input-dynamic.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormDynamicComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_inputservicebase__ = __webpack_require__(285);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the FormDynamicComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var FormDynamicComponent = /** @class */ (function () {
    function FormDynamicComponent(_questionControlService) {
        this._questionControlService = _questionControlService;
        this.inputdinamico = [];
        this.values = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.payLoad = '';
        console.log('Hello FormDynamicComponent Component');
    }
    FormDynamicComponent.prototype.ngOnInit = function () {
        this.form = this._questionControlService.toFormGroup(this.inputdinamico);
    };
    FormDynamicComponent.prototype.onSubmit = function () {
        this.payLoad = JSON.stringify(this.form.value);
        this.values.emit(this.form.value);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Array)
    ], FormDynamicComponent.prototype, "inputdinamico", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], FormDynamicComponent.prototype, "values", void 0);
    FormDynamicComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'form-dynamic',template:/*ion-inline-start:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/components/form-dynamic/form-dynamic.html"*/'<div>\n  \n  <form (ngSubmit)="onSubmit()" [formGroup]="form">\n\n    <div *ngFor="let inputdinamico of inputdinamico" class="form-row">\n      <input-dinamico [inputdinamico]="inputdinamico" [form]="form"></input-dinamico>\n    </div>\n\n    <div class="form-row">\n      <button type="submit" [disabled]="!form.valid">Enviar Petición</button>\n    </div>\n  </form>\n\n  <div *ngIf="payLoad" class="form-row">\n    <strong>Saved the following values</strong><br>{{payLoad}}\n  </div>\n\n\n</div>\n'/*ion-inline-end:"/Users/everis-1/Developer/ionic/PROJECT01/App-ABI/src/components/form-dynamic/form-dynamic.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_1__services_inputservicebase__["a" /* QuestionControlService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_inputservicebase__["a" /* QuestionControlService */]])
    ], FormDynamicComponent);
    return FormDynamicComponent;
}());

//# sourceMappingURL=form-dynamic.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionControlService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var QuestionControlService = /** @class */ (function () {
    function QuestionControlService() {
    }
    QuestionControlService.prototype.toFormGroup = function (inputdinamico) {
        var group = {};
        inputdinamico.forEach(function (question) {
            group[question.key] = question.required ? new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */](question.value || '', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required)
                : new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */](question.value || '');
        });
        return new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */](group);
    };
    QuestionControlService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], QuestionControlService);
    return QuestionControlService;
}());

//# sourceMappingURL=inputservicebase.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReNewPasswordSF; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_types_inputs_input_etn__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ReNewPasswordSF = /** @class */ (function () {
    function ReNewPasswordSF() {
    }
    // TODO: get from a remote source of question metadata
    // TODO: make asynchronous
    ReNewPasswordSF.prototype.getQuestions = function () {
        var inputs = [
            /*
            new DropdownQuestion({
              key: 'brave',
              label: 'Bravery Rating',
              options: [
                {key: 'solid',  value: 'Solid'},
                {key: 'great',  value: 'Great'},
                {key: 'good',   value: 'Good'},
                {key: 'unproven', value: 'Unproven'}
              ],
              order: 3
            }),
            */
            new __WEBPACK_IMPORTED_MODULE_1__providers_types_inputs_input_etn__["a" /* TextboxQuestion */]({
                key: 'userId',
                label: 'Usuario SalesForce',
                required: true,
                order: 1
            }),
        ];
        return inputs.sort(function (a, b) { return a.order - b.order; });
    };
    ReNewPasswordSF = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], ReNewPasswordSF);
    return ReNewPasswordSF;
}());

//# sourceMappingURL=renewpwdSFDC.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_types_inputs_input_etn__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var QuestionService = /** @class */ (function () {
    function QuestionService() {
    }
    // TODO: get from a remote source of question metadata
    // TODO: make asynchronous
    QuestionService.prototype.getQuestions = function () {
        var inputs = [
            /*
            new DropdownQuestion({
              key: 'brave',
              label: 'Bravery Rating',
              options: [
                {key: 'solid',  value: 'Solid'},
                {key: 'great',  value: 'Great'},
                {key: 'good',   value: 'Good'},
                {key: 'unproven', value: 'Unproven'}
              ],
              order: 3
            }),
            */
            new __WEBPACK_IMPORTED_MODULE_1__providers_types_inputs_input_etn__["a" /* TextboxQuestion */]({
                key: 'userId',
                llave: 'userId',
                label: 'Usuario SAP',
                required: true,
                order: 1
            }),
        ];
        return inputs.sort(function (a, b) { return a.order - b.order; });
    };
    QuestionService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])()
    ], QuestionService);
    return QuestionService;
}());

//# sourceMappingURL=formdinamico.js.map

/***/ })

},[204]);
//# sourceMappingURL=main.js.map