import { NgModule } from '@angular/core';
import { InputDynamicComponent } from './input-dynamic/input-dynamic';
import { FormDynamicComponent } from './form-dynamic/form-dynamic';


import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
@NgModule({
	declarations: [
	FormDynamicComponent,
	InputDynamicComponent],
  imports: [CommonModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    IonicModule,
    IonicApp,
    IonicErrorHandler,
    
   ],
	exports: [
    FormDynamicComponent,
    InputDynamicComponent]
})
export class ComponentsModule {}
