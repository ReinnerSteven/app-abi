import { Component, EventEmitter, Input, Output, OnInit }  from '@angular/core';
import { FormGroup } from '@angular/forms';



import { InputBase } from "../../providers/types-inputs/inputs";
import { QuestionControlService } from "../../services/inputservicebase";
/**
 * Generated class for the FormDynamicComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'form-dynamic',
  templateUrl: 'form-dynamic.html',
  providers: [ QuestionControlService ]
})
export class FormDynamicComponent implements OnInit {

  @Input() inputdinamico: InputBase<any>[] = [];
  @Output() values = new EventEmitter();


  form: FormGroup;
  payLoad = '';

  constructor(private _questionControlService: QuestionControlService) {
    console.log('Hello FormDynamicComponent Component');
  }
  
  ngOnInit() {
    this.form = this._questionControlService.toFormGroup(this.inputdinamico);
  }
  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
    this.values.emit(this.form.value);
  }
}
