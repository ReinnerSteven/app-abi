
import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class DeviceCameraProvider {


   /**
   * Variable que contiene la configuracion para cargar la camara
  */
  private options: CameraOptions = {
    quality: 100,
    correctOrientation: true,
    mediaType: this.camera.MediaType.PICTURE,
    encodingType: this.camera.EncodingType.JPEG,
    destinationType: this.camera.DestinationType.DATA_URL,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  }

  constructor(public camera: Camera) {}

  /**
   * Funcion que se encarga de activar el acceso a la camara
   * @param { Boolean}  [openGallery]  Define si se carga la camara o la galeria
   * @returns {Promise<any>} Returns una Promise la imagen en base 64
   * el timestamp de donde se solicito
  */
  openCamera(openGallery:boolean): Promise<any>{
    var prom = new Promise((resolve,reflect)=> {
      this.options.sourceType =  !openGallery ?  this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA
      this.camera.getPicture(this.options).then((imageData) => {  
        resolve(`data:image/jpeg;base64,${imageData}`);
       }, (err) => {
         reflect(err);
       });
    })
    return prom;
  }
}
