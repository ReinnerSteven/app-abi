import { Geolocation } from '@ionic-native/geolocation';
import { Injectable } from '@angular/core';

@Injectable()
export class DeviceLocationProvider {

  constructor(public _geolocation: Geolocation) {}

  /**
   * Funcion que se encarga de optener la posicion actual del usuario   *
   * @returns {Promise<any>} Returns una Promise que contiene la posicion del usuario y 
   * el timestamp de donde se solicito
  */
  currentLocation(): Promise<any>{
     var prom = new Promise((resolve,reflect)=> {
      this._geolocation.getCurrentPosition().then((resp) => {
       resolve(resp);
      }).catch((err) => {
       reflect(err);
      });
   })
   return prom;
  }
}
