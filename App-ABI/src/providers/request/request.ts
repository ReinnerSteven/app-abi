import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { RequestModel, ServicesModel, RequestType, QueryModel } from "../../models/model.request";


@Injectable()

export class RequestProvider {

  constructor(
    public _httpClient: HttpClient,
    public _servicesModel: ServicesModel
  ) {}


  private _headers = new HttpHeaders({
    "Authorization": "Basic YWRtaW46YWRtaW4=",
    "Content-Type": "application/json"
  })

   /**
   * Funcion que se encarga de realizar una peticion HTTP al servidor
   * @param {RequestModel}  typeRequest Tipo de SOLICITUD que se quiere realizar GET,POST, PUT 
   * @param {RequestType}  typeServcie Tipo de SERVICIO que se quiere realizar ( endpoints)
   * @param {any}  body Parametros tipos JSON para enviar en la solicitud. si este campo no va, colocar null
   * @param {QueryModel}  query Parametros para agregar a la url, estos deben de estar igual
   * al que se implementa en la interface. si este campo no va, colocar null
   * @returns {Promise<any>} Returns una Promise que contiene la posicion del usuario y 
   * el timestamp de donde se solicito
  */
  request(typeRequest:RequestModel, typeServcie:RequestType, body:any, query:QueryModel){
    let url = this._servicesModel.urlRequest(typeServcie,query)
    console.log("\n---> RequestModel: ",typeRequest)
    console.log("\n---> URL: ",url)
    console.log("\n---> PARAMS: ",body)
    console.log("\n---> HEADERS: ",this._headers)
    console.log("\n---> QUERY: ",query)

    let request = new HttpRequest(
      typeRequest,url, 
      JSON.stringify(body), { 
      headers:this._headers,
    })
    return this._httpClient.request(request).toPromise()
  }

    /**
   * Funcion que se encarga de realizar una peticion HTTP al servidor
   * @param {RequestModel}  typeRequest Tipo de SOLICITUD que se quiere realizar GET,POST, PUT 
   * @param {RequestType}  typeServcie Tipo de SERVICIO que se quiere realizar ( endpoints)
   * @param {any}  body Parametros tipos JSON para enviar en la solicitud
   * @param {QueryModel}  query Parametros para agregar a la url, estos deben de estar igual
   * al que se implementa en la interface
   * 
   * 
   * Para implementar este metodo de progreso debemos suscribirnos a los eventos
   * 
   *  .subscribe(event => {
        if (event.type === HttpEventType.DownloadProgress) {
          let port = (event.loaded / event.total) * 100
          console.log(port);
        }
        
        if (event.type === HttpEventType.UploadProgress) {
          let port = (event.loaded / event.total) * 100
          console.log(port);
        }

        if (event.type === HttpEventType.Response) {
          console.log(event.body);
        }
      }, (error:HttpErrorResponse) =>{
        console.log(error);
      })
   * 
   * 
   * @returns {Promise<any>} Returns una Promise que contiene la posicion del usuario y 
   * el timestamp de donde se solicito
  */
  requestProgress(typeRequest:RequestModel, typeServcie:RequestType, body:any, query:QueryModel){
    let url = this._servicesModel.urlRequest(typeServcie,query)
    let request = new HttpRequest(
      typeRequest, 
      url, 
      { 
        headers:this._headers
      })
    return this._httpClient.request(request)
  }
}
