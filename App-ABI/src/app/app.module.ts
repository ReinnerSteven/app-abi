import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

//Device
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { DeviceCameraProvider } from '../providers/device-camera/device-camera';
import { DeviceLocationProvider } from '../providers/device-location/device-location';
import { RequestProvider } from '../providers/request/request';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ServicesModel } from '../models/model.request';

//Dynamic Form
import { InputDynamicComponent } from '../components/input-dynamic/input-dynamic';
import { FormDynamicComponent } from '../components/form-dynamic/form-dynamic';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ReNewPasswordSF } from '../services/renewpwdSFDC';
import { QuestionService } from '../services/formdinamico';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FormDynamicComponent,
    InputDynamicComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    Geolocation,
    DeviceCameraProvider,
    DeviceLocationProvider,
    RequestProvider,
    ServicesModel,
    QuestionService, 
    ReNewPasswordSF
    
  ]
})
export class AppModule {}
