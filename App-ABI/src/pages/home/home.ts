import { TextboxQuestion } from './../../providers/types-inputs/input-etn';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { DeviceLocationProvider } from '../../providers/device-location/device-location'
import { DeviceCameraProvider } from '../../providers/device-camera/device-camera'
import { RequestProvider } from '../../providers/request/request';
import { RequestModel, RequestType, QueryModel } from '../../models/model.request';

import { QuestionService } from '../../services/formdinamico';
import { ReNewPasswordSF } from "../../services/renewpwdSFDC";
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  locationUser:any;
  inputdinamico: any[];
  formrenewpwdSF: any[];
  status: any;
  result: any


  constructor(public navCtrl: NavController,
    private _camera: DeviceCameraProvider, 
    private _geolocation: DeviceLocationProvider,
    private _request:RequestProvider,
    private _service: QuestionService,
    private _renewpwdSF: ReNewPasswordSF,
    public loadingCtrl: LoadingController,
  ) 
    {
      
      console.log("--> Start Home\n");
      this.inputdinamico = _service.getQuestions();
      this.formrenewpwdSF = _renewpwdSF.getQuestions()

  }

  public imagePath :any;

  openCamera(openCamera:boolean){
    console.log(openCamera)
     this._camera.openCamera(openCamera).then((src)=>{
       this.imagePath = src;
     }).catch((err) => {
       console.log(err);
     })
  }

  loadlocation(){
    this._geolocation.currentLocation().then((location)=>{
      this.locationUser = location
    }).catch((err)=>{
      console.log(err)
    })
  }

  unblockSAP(event){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();
    let query:QueryModel  = event
      let data = {
        "metaData": {
          "msgId": "XMKMSIJD",
          "user": "test",
          "source": "app/Web/ChatBot",
          "target": "PR3/HCM/SF/Compaxx/SharpOps/ServiceNow/Datamart",
          "transactionDate": "2017-08-10T09:05:49.999"
        },
        "data": []
      }
  
      this._request.request(RequestModel.PUT,RequestType.SAP_UNLOCK_USER,data,query)
      .then((data:any) => {
        loading.dismiss();
        this.result = JSON.stringify(data.body.data)
      })
      .catch(error => {
        console.log(error)
        loading.dismiss();

      });
  }
  resetPasswordSFDC(event){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    console.log("reset password");
    let query:QueryModel  = event
      let data = {
        "metaData": {
          "msgId": "XMKMSIJD",
          "user": "test",
          "source": "app/Web/ChatBot",
          "target": "PR3/HCM/SF/Compaxx/SharpOps/ServiceNow/Datamart",
          "transactionDate": "2017-08-10T09:05:49.999"
        },
        "data": []
      }
  
      this._request.request(RequestModel.PUT,RequestType.SALEFORCE_RESET_PASS,data,query)
      .then((data:any) => {
        loading.dismiss();
        console.log(data)
        this.result = JSON.stringify(data.body.data)
      })
      .catch(error => {
        console.log(error)
        loading.dismiss();
      });
  }

}
