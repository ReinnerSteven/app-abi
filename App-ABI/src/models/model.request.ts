import { Injectable } from '@angular/core';

/**
 * Tipos de servicios que se van a realizar
 */
export enum RequestModel {
  GET = "GET",
  PUT = "PUT",
  POST = "POST"
}

/**
 * Enum enfocado a los tipos de servicios que se van hacer
 */
export enum RequestType {
  //SALEFORCE
  SALEFORCE_RESET_PASS,   //Valor 0
  //SAP
  SAP_UNLOCK_USER,        //Valor 1
  SAP_VALIDATE_USER       //Valor 2
}

/**
 * Enum enfocado a los querys que van por http,
 */
export interface QueryModel {
  userId: string,
  nickname: string
}

@Injectable()


export class ServicesModel {
  /**
   * Funcion que se encarga de validar que tipo de solicitud se va a realizar 
   * @param {RequestType}  typeServcie Tipo de SERVICIO que se quiere realizar ( endpoints)
   * @param {QueryModel}  query Parametros para agregar a la url, estos deben de estar igual
   * al que se implementa en la interface. si este campo no va, colocar null
   * @returns {String} Returns url para realizar el servicio
   */
  urlRequest(typeServcie: RequestType, query: QueryModel) {
    console.log(typeServcie)
    switch (typeServcie) {
      case 0: {
        return `https://caro-chatboot.de-c1.cloudhub.io/api/1.1/accounts/${query.userId}/resetPassword`;
      }
      case 1: {
        return `https://caro-chatboot.de-c1.cloudhub.io/api/1.1/employees/${query.userId}/unblock`;
      }
      case 2: {
        return `https://caro-chatboot.de-c1.cloudhub.io/api/1.1/employees/${query.userId}/validate`;
      }
      default: {
        console.log("Invalid endpoint");
        break;
      }
    }
  }
}